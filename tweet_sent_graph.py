import twitter_settings as settings
import json
import sqlite3
import pandas as pd
import plotly.graph_objects as go
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

#connect to sql 
conn = sqlite3.connect('twitter.db')
c = conn.cursor()


def sentiment_graph(keyword):

    df = pd.read_sql("SELECT * FROM sentiment ORDER BY created_at DESC LIMIT 1000", conn)
    df.sort_values('created_at', inplace=True)
    df["compound_ma"] = df["compound"].rolling(50).mean()

    fig = go.Figure()

    fig.add_trace(go.Scatter(x=df["created_at"], y=df["compound"], mode='markers', name="Compound Sentiment",
        marker={"size":10, "line":{"width":1}, "colorscale":"plasma", "color":df["compound"]}, hovertext=df['tweet']))
    fig.add_trace(go.Scatter(x=df["created_at"], y=df["compound_ma"], mode='lines', name="Moving Average",
        line={"width":3, "color":"green"}, line_shape='linear'))

    fig.update_layout(title=f'VADER Compound Sentiment of recent Tweets about: {keyword}',
                       xaxis_title='Tweet Time',
                       yaxis_title='Compound Sentiment')

    fig.write_image("sent_graph.png")
    fig.write_html("sent_graph.html", include_plotlyjs='cdn')

sentiment_graph(settings.keyword)

def tweet_cloud(keyword):
	df = pd.read_sql("SELECT * FROM sentiment ORDER BY created_at DESC LIMIT 1000", conn)
	df.sort_values('created_at', inplace=True)
	text = " ".join(tweet for tweet in df["tweet"])
	stopwords = set(STOPWORDS)
	stopwords.update(["t", "co", "http", "https", "rt", keyword])
	wordcloud = WordCloud(stopwords=stopwords, background_color="white", width=800, height=400).generate(text)
	wordcloud.to_file("tweet_cloud.png")

tweet_cloud(settings.keyword)
