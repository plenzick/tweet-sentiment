# Tweet Sentiment

Pair of scripts to collect tweets about a topic and generate a sentiment visualization

## Usage

The `twitter_settings.py` file should be edited to include your twitter API keys and desired keyword.

`tweet_scrape.py` will scrape twitter for 100 tweets about the desired topic (edit number as needed), and can be run as many times as desired to collect the needed number and time span of tweets. The text of the tweets is analyzed with the VADER sentiment package. Tweets and sentiment values are stored in a SQL database and subsequent runs will just add to the same database.

`tweet_sent_graph.py` can be run once you have enough tweets to create a word cloud and a moving average sentiment timeline for the chosen keyword.

## Example output

![sentiment graph](sent_graph.png)

![word cloud](tweet_cloud.png)





