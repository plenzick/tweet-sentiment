import tweepy as tw
import twitter_settings as settings
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from datetime import datetime, timedelta
import json
import sqlite3
import pandas as pd

analyzer = SentimentIntensityAnalyzer()

#connect to sql and create new table if necessary
conn = sqlite3.connect('twitter.db')
c = conn.cursor()

def create_table():
	c.execute("""CREATE TABLE IF NOT EXISTS sentiment(created_at DATETIME, tweet TEXT, 
		positive REAL, neutral REAL, negative REAL, compound REAL)""")
	conn.commit()

create_table()

#twitter API keys
ckey=settings.ckey
csecret=settings.csecret
atoken=settings.atoken
asecret=settings.asecret

auth = tw.OAuthHandler(ckey, csecret)
auth.set_access_token(atoken, asecret)
api = tw.API(auth, wait_on_rate_limit=True)

def get_tweets(search_words, num_items=100, search_lang="en"):
	'''search tweets of a given keyword, analyzes sentiment and inserts time,
	text, and sentiment value to sql database'''
	tweets = tw.Cursor(api.search_tweets,
		q=search_words,
		lang=search_lang).items(num_items)
	for tweet in tweets:
		sentiment = analyzer.polarity_scores(tweet.text)
		#exclude tweets that don't have a sentiment score
		if sentiment['compound']!=0:
			#print(tweet.text, sentiment, tweet.created_at)
			c.execute("""INSERT INTO sentiment (created_at, tweet, positive, neutral, negative, compound) 
				VALUES (?, ?, ?, ?, ?, ?)""", 
				(tweet.created_at, tweet.text, sentiment['pos'], sentiment['neu'], sentiment['neg'], sentiment['compound']))
			conn.commit()

get_tweets(settings.keyword, 1000)
